// HW#5
// Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования: 
// Ответ: Экранирование символов — замена в тексте управляющих символов на соответствующие текстовые подстановки. То есть благодаря экранированию мы можем символы, которые могут выполняют определённые действия в синтаксисе отобразить в виде текста/строки если нам это нужно, при этом эти символы перестанут выполнять роль символа, а будут видны как текст.


console.log(createNewUser().getPassword());
function createNewUser() {
const userName = prompt("Please, enter your first name");
const userSerName = prompt("Please, enter your last name");
const userAge = prompt("Please, enter your date of birth", "dd.mm.yyyy");
    const newUser = {
    firstName : userName,
    lastName : userSerName, 
    birthday : userAge,
    getLogin() {
        return (this.firstName[0] + this.lastName).toLowerCase();
    },
    getAge() {
        const currentDate = new Date();
        const currentYear = currentDate.getFullYear();
        const userYear = this.birthday.slice(-4);
        return currentYear - userYear;
    },
    getPassword() {
        return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4);
    },
}
return newUser;
}